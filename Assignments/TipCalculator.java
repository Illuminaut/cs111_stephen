import java.util.Scanner ;
public class TipCalculator {
	public static void main(String []args) {
		Scanner UserInput = new Scanner (System.in) ;
		System.out.println ("Tip Calculator by Stephen, Date TBD") ;
		System.out.println ("Please input your name") ;
		String Username = UserInput.nextLine() ;
		System.out.println (Username + ", Welcome to the Tip Calculator") ;
		System.out.println ("Please enter your bill amount. Note that if you do not type a number, the program will crash. Do not enter dollar signs or symbols of any sort.") ;
		double PriceAmount = UserInput.nextDouble() ; 
		System.out.println ("Please enter the number of people in your party that you would like to split the bill with.") ;
		int NumberOfPeople = UserInput.nextInt () ;
		System.out.println ("Please input the percentage of the bill you would like to tip in 0 to 100 percent. Do not enter percent signs or symbols of any sort.") ;
		int TipPercentage = UserInput.nextInt () ;
		double TipBeforeRounding = PriceAmount*(TipPercentage/100.0) ;
		System.out.print ("Your tip total comes to:" ) ;
		double Tip100 = (TipBeforeRounding*100) ;
		int RoundedTip =(int)Math.round (Tip100) ;
		double FinalTip = (RoundedTip/100.0) ;
		System.out.print (FinalTip) ;
		System.out.println ("     The price of the tip will be split among the final bill") ;
		double FinalPrice = (PriceAmount+FinalTip)/NumberOfPeople ;
		double RoundedPricePreDivision = (FinalPrice*100) ;
		int Rounded=(int)Math.round (RoundedPricePreDivision)  ; 
		double RoundedPricePostDivision = (Rounded/100.0) ;
		System.out.print ("Your final price per person comes to:") ;
		System.out.println (RoundedPricePostDivision) ;


	}

}