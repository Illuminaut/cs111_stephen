import java.util.Date;

public class Main
{
    public static void main(String[] args)
    {
		
		System.out.println ("Stephen Kinnear\n") ;
        // Variables:
        Octopus ocky;           
        Utensil spat; 
		Utensil tong ;
		Octopus oct2 ;

        spat = new Utensil("spatula"); // create a utensil with the name spatula
        spat.setColor("green");        // set spatula properties--color...
        spat.setCost(10.59);    
		
		tong = new Utensil("tongs"); // create a utensil with the name spatula
        tong.setColor("silver");        // set spatula properties--color...
        tong.setCost(11.87);

        ocky = new Octopus("Ocky",13);    // create and name the octopus               // set the octopus's age...
        ocky.setWeight(100);           // ... weight,...
        ocky.setUtensil(spat);         // ... and favorite utensil.

		oct2 = new Octopus("Octocon",17);    // create and name the octopus
        oct2.setWeight(145);           // ... weight,...
        oct2.setUtensil(tong);
		
        System.out.println("Testing 'get' methods:");
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight() + " pounds");
		System.out.print("and is " + ocky.getAge() + " years old. ");
		System.out.println("His favorite utensil is a " + ocky.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $" + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());
		
		 System.out.println("Testing 'get' methods:");
        System.out.println(oct2.getName() + " weighs " +oct2.getWeight() + " pounds");
		System.out.print("and is " + oct2.getAge() + " years old. ");
		System.out.println("His favorite utensil is a " + oct2.getUtensil());

        System.out.println(oct2.getName() + "'s " + oct2.getUtensil() + " costs $" + oct2.getUtensil().getCost());
        System.out.println("Utensil's color: " + tong.getColor());

        // Use methods to change some values:

        ocky.setAge(20);
        ocky.setWeight(125);
        spat.setCost(15.99);
        spat.setColor("blue");

        System.out.println("\nTesting 'set' methods:");
        System.out.println(ocky.getName() + "'s new age: " + ocky.getAge());
        System.out.println(ocky.getName() + "'s new weight: " + ocky.getWeight());
        System.out.println("Utensil's new cost: $" + spat.getCost());
        System.out.println("Utensil's new color: " + spat.getColor());
    }
}
