import java.util.Scanner ;
public class MathAndBooleans {
	public static void main(String[] args){
		Scanner UserInput = new Scanner(System.in) ;
		System.out.print ("Please type in a year between 1000 and 3000:  ") ;
		int YearInput = UserInput.nextInt() ;
		if (YearInput > 3000 ) { 
			YearInput = 3000 ; 
			System.out.println ("Nice try. I'm setting it to 3000") ; 
		} else if (YearInput < 1000) {
			YearInput = 1000 ;
			System.out.println ("Nice try. I'm setting it to 1000") ;
		} else { System.out.println ("Congratulations. You managed to follow the instructions without hurting yourself.") ; 
		}  
		
		
		// Testing for leap year here
		if (YearInput %4 ==0) {
			if (YearInput %100 ==0) {
				if  (YearInput %400 ==0) {
					System.out.println (YearInput+" is a leap year") ;
					
				} else {
					System.out.println (YearInput+" is not a leap year") ;
				}
				
				
			} else {
				System.out.println (YearInput+" is a leap year") ;
			}
			
			
		} else {
			System.out.println (YearInput+" is not a leap year") ;
		}
		
	// Testing for 17 Year Cicadas (Brood II)
		if ((YearInput-2013) %17 == 0){
			System.out.println (YearInput+" is a year with the emergence of the 17 Year Cicadas") ;
		} else System.out.println (YearInput+" is not a year with the emergence of the 17 Year Cicadas") ;
		
		if ((YearInput-2013) %11 == 0){
			System.out.println (YearInput+" is a year with increased sunspot activity") ;
		} else System.out.println (YearInput+" is not a year with increased sunspot activity") ;
	}	
}